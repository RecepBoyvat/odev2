﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(webodevilayout.Startup))]
namespace webodevilayout
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
