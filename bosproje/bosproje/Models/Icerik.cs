﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace bosproje.Models
{
    public class Icerik
    {
        [Key]
        public int ProjeId { get; set; }

        public string ProjeAdi { get; set; }

        public string ProjeAciklamasi { get; set; }

       
        public string ProjeSahibi { get; set; }
    }
}