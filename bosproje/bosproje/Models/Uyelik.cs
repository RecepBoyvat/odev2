﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace bosproje.Models
{
    public class Uyelik
    {
        [Key]
        public int UyeId { get; set; }
        public string Ad { get; set; }

        public string Soyad { get; set; }

        public string Email { get; set; }

        public string Sifre{ get; set; }

        public bool yetki { get; set; }


    }
}