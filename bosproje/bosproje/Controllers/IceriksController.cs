﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using bosproje.DAL;
using bosproje.Models;

namespace bosproje.Controllers
{
    public class IceriksController : Controller
    {
        private ProjeContext db = new ProjeContext();

        // GET: Iceriks
        public ActionResult Index()
        {
            return View(db.Icerikler.ToList());
        }

        // GET: Iceriks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Icerik icerik = db.Icerikler.Find(id);
            if (icerik == null)
            {
                return HttpNotFound();
            }
            return View(icerik);
        }

        // GET: Iceriks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Iceriks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProjeId,ProjeAdi,ProjeAciklamasi,ProjeSahibi")] Icerik icerik)
        {
            if (ModelState.IsValid)
            {
                db.Icerikler.Add(icerik);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(icerik);
        }

        // GET: Iceriks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Icerik icerik = db.Icerikler.Find(id);
            if (icerik == null)
            {
                return HttpNotFound();
            }
            return View(icerik);
        }

        // POST: Iceriks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ProjeId,ProjeAdi,ProjeAciklamasi,ProjeSahibi")] Icerik icerik)
        {
            if (ModelState.IsValid)
            {
                db.Entry(icerik).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(icerik);
        }

        // GET: Iceriks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Icerik icerik = db.Icerikler.Find(id);
            if (icerik == null)
            {
                return HttpNotFound();
            }
            return View(icerik);
        }

        // POST: Iceriks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Icerik icerik = db.Icerikler.Find(id);
            db.Icerikler.Remove(icerik);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
