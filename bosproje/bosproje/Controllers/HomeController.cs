﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace bosproje.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Kayit()
        {
            return View();
        }
        public ActionResult Icerik()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Benim Hakkımda.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Iletisim Bilgilerim.";

            return View();
        }
    }
}