﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using bosproje.DAL;
using bosproje.Models;


namespace bosproje.Controllers
{
    public class UyeliksController : Controller
    {
        private ProjeContext db = new ProjeContext();

        // GET: Uyeliks
        public ActionResult Index()
        {
            return View(db.Kayit.ToList());
        }

        // GET: Uyeliks/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uyelik uyelik = db.Kayit.Find(id);
            if (uyelik == null)
            {
                return HttpNotFound();
            }
            return View(uyelik);
        }

        // GET: Uyeliks/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Uyeliks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UyeId,Ad,Soyad,Email,Sifre,yetki")] Uyelik uyelik)
        {
            if (ModelState.IsValid)
            {
                db.Kayit.Add(uyelik);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(uyelik);
        }

        // GET: Uyeliks/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uyelik uyelik = db.Kayit.Find(id);
            if (uyelik == null)
            {
                return HttpNotFound();
            }
            return View(uyelik);
        }

        // POST: Uyeliks/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UyeId,Ad,Soyad,Email,Sifre,yetki")] Uyelik uyelik)
        {
            if (ModelState.IsValid)
            {
                db.Entry(uyelik).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(uyelik);
        }

        // GET: Uyeliks/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Uyelik uyelik = db.Kayit.Find(id);
            if (uyelik == null)
            {
                return HttpNotFound();
            }
            return View(uyelik);
        }

        // POST: Uyeliks/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Uyelik uyelik = db.Kayit.Find(id);
            db.Kayit.Remove(uyelik);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
