﻿using bosproje.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace bosproje.DAL
{
    public class ProjeContext:DbContext
    {
        public DbSet<Uyelik> Kayit { get; set; }
        public DbSet<Icerik> Icerikler { get; set; }

        public ProjeContext():base("ProjeContext"){ }
        
        
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }



    }
      

    
    } 
